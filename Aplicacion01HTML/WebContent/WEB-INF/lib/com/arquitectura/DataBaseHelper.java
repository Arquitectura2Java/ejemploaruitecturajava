package com.arquitectura;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;


public class DataBaseHelper<T> {

		private static final String DRIVER="com.mysql.cj.jdbc.Driver";
		private static final String URL ="jdbc:mysql://localhost/arquitecturajava"+
				"?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		private static final String USUARIO ="root";
		private static final String CLAVE = "g00r002b";
		
		private static final Logger log = Logger.getLogger(DataBaseHelper.class.getPackage().getName());
		public DataBaseHelper()
		{
			
		}
		public int modificarRegistro(String consultaSQL) throws ClassNotFoundException, 
		SQLException{
				Connection conexion = null;
				Statement sentencia = null;
				int filasAfectadas = 0 ;
				
				try {
					Class.forName(DRIVER);
					conexion= DriverManager.getConnection(URL, USUARIO, CLAVE);
					sentencia= conexion.createStatement();
					filasAfectadas = sentencia.executeUpdate(consultaSQL);
					
				}catch(ClassNotFoundException e ) {
					log.error("Error driver " + e.getMessage() );
					throw e ;
				}catch(SQLException e ) {
					log.error("Error de SQL " + e.getMessage());
					throw e;
				} finally {
						
					if(sentencia !=null)
					{
						try{ sentencia.close();}catch(SQLException e ) {}
					}
					
					if(conexion != null ) {
						try{conexion.close();}catch(SQLException e ) {}
					}
				}
				
				return filasAfectadas;
		}
		
		public List<T> seleccionarRegistros(String consultaSQL, Class clase) throws ClassNotFoundException,
		SQLException, IllegalAccessException, InvocationTargetException, InstantiationException {
			Connection conexion = null;
			Statement sentencia = null;
			ResultSet filas = null;
			List<T> listaDeObjetos = new ArrayList<T>();
			
			try {
				Class.forName(DRIVER);
				conexion = DriverManager.getConnection(URL, USUARIO, CLAVE);
				sentencia = conexion.createStatement();
				filas = sentencia.executeQuery(consultaSQL);
				
				while(filas.next()){
					T objeto=(T)Class.forName(clase.getName()).newInstance();
					Method[] metodos=objeto.getClass().getDeclaredMethods();
					
					for(int i=0; i<metodos.length; i++)
					{
						if(metodos[i].getName().startsWith("set"))
						{
							metodos[i].invoke(objeto, 
							filas.getString(metodos[i].getName().substring(3)));
						}
						
						if(objeto.getClass().getName().equals("java.lang.String")) {
							objeto=(T)filas.getString(1);
						}
					}
					listaDeObjetos.add(objeto);
				}
			}catch( ClassNotFoundException e ){log.error("Error al seleccionar registros " + e.getMessage());
					throw e;
			}catch(SQLException e){
				log.error("Error en la sentencia SQL " + e.getMessage());
				throw e;
			}catch(IllegalAccessException e){
				log.error("Error error reflection " + e.getMessage());
				throw e;
			}catch(InvocationTargetException e) {
				log.error("Error reflection " + e.getMessage());
				throw  e;
			}catch(InstantiationException e ){
				log.error("Error reflection  " + e.getMessage());
				throw e;
			}finally {
			
				if(sentencia !=null)
				{
					try {
						sentencia.close();}catch(SQLException e ){
							
							log.error("Error al cerrar la sentencia " + e.getMessage());
							}
					}
				
					if(conexion != null)
					{
						try {conexion.close();}catch(SQLException e ){log.error("Error al cerrar la conexion " + e.getMessage());
						}
					}
				}
			
			return listaDeObjetos;
		}
		
		
}
