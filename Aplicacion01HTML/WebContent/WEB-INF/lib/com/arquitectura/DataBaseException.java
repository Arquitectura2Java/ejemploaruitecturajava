package com.arquitectura;

public class DataBaseException extends RuntimeException {
	private static final long serialVersionUID=1L;
	//kss
	public DataBaseException() {
		super();
	}
	
	public DataBaseException(String message, Throwable cause){
		super(message, cause);
	}
	
	public DataBaseException(String message) {
		super( message);
	}
	
	public DataBaseException(Throwable cause) {
		super(cause);
	}
	public Throwable getException(){
		return super.getCause();
	}
	

}
