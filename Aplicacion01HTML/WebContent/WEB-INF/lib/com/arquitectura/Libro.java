package com.arquitectura;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
public class Libro {
	private String isbn;
	private String titulo;
	private String categoria;
	
	public Libro() {
		this.isbn = "";
		this.titulo = "";
		this.categoria= "";
	}
	public Libro(String isbn, String titulo, String categoria)
	{
		this.categoria = categoria;
		this.titulo = titulo;
		this.isbn = isbn;
	}
	public Libro(String isbn)
	{
		this.isbn=isbn;
		this.titulo="";
		this.categoria="";
			
	}
	public static List<String> buscarTodasLasCategorias() 
	{
		String consultaSQL = "select distinct(Categoria) from Libros";
		DataBaseHelper<String> helper= new DataBaseHelper<String>();
		
		List<String> listaCategorias;
		try{ 
			listaCategorias = helper.seleccionarRegistros(consultaSQL,String.class);
		}catch(ClassNotFoundException e) {
			throw new DataBaseException("Error al leer el driver", e );
		}catch(SQLException e){
			throw new DataBaseException("Error al ejecutar consulta", e );
		}catch(IllegalAccessException e){
			throw new DataBaseException("Error acceso incorrecto a la clase", e );
		}catch(InvocationTargetException e ){
			throw new DataBaseException("Error reflection", e );
		}catch(InstantiationException e){
			throw new DataBaseException("Error reflection", e );
		}
		

		return listaCategorias;
	}
	
	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	

	public  void insertar() {
		String consultaSQL = "insert into Libros(isbn,Titulo,Categoria) values";
		consultaSQL +="('" + isbn + "','" + titulo + "','" + categoria + "')";
		DataBaseHelper helper = new DataBaseHelper();
		try {
		helper.modificarRegistro(consultaSQL);
		}catch(SQLException e )
		{
			System.out.println("Error al insertar  SQL" + e.getMessage());
			throw new DataBaseException("Error al insertar un libro  ", e);
		}catch(ClassNotFoundException e ) {
			System.out.println("Error al acceder el driver " + e.getMessage());
			throw new DataBaseException("Error al acceder el driver",e);
		}
	}
	
	public static  List<Libro> buscarTodos() 
	{
		
		String consultaSQL = "select isbn, Titulo, Categoria from Libros";
		DataBaseHelper helper= new DataBaseHelper();
		List<Libro> listaDeLibros=null;
		
		try{listaDeLibros = helper.seleccionarRegistros(consultaSQL,Libro.class);
		}catch(ClassNotFoundException e) {
			throw new DataBaseException("Error al leer el driver", e );
		}catch(SQLException e){
			System.out.println("Entrando a la exception DataBaseException");
			throw new DataBaseException("Error al ejecutar consulta", e );
		}catch(IllegalAccessException e){
			throw new DataBaseException("Error acceso incorrecto a la clase", e );
		}catch(InvocationTargetException e ){
			throw new DataBaseException("Error reflection", e );
		}catch(InstantiationException e){
			throw new DataBaseException("Error reflection", e );
		}
		return listaDeLibros;
	}
	
	public void borrar() 
	{
		String consultaSQL = "delete from libros where isbn='"+ this.isbn+"'";
		DataBaseHelper<Libro> helper= new DataBaseHelper<Libro>();
		
		try{helper.modificarRegistro(consultaSQL);}catch(SQLException e ){
			System.out.println("Error en consulta borrar libro" + e. getMessage() );
			throw new DataBaseException("Error al borrar un libro ", e);
		}catch(ClassNotFoundException e ) {
			System.out.println("Error en borrar libro"+ e.getMessage());
			throw new DataBaseException("Error no se encuentra la clase al borrar un libro ", e);
		}
	}
	
	public void salvar()  {
		String consultaSQL = "update libros set titulo='" + this.titulo+
				"', categoria='"+ this.categoria+"' where isbn='" + this.isbn+ "'";
		DataBaseHelper<Libro> helper = new DataBaseHelper<Libro>();
		
		try{helper.modificarRegistro(consultaSQL);}catch(SQLException e ) {
			System.out.println("Error en la consulta guardar  registro" + e.getMessage());
			throw new DataBaseException("Error al guardar un libro ",e);
		}catch(ClassNotFoundException e ) {
			System.out.println("Error al guardar registro" + e.getMessage());
			throw new DataBaseException("No se encuentra la clase libro al momento de guardarla", e);
		}
	}
	
	public static Libro buscarPorClave(String isbn) 
	{
		String consultaSQL="select isbn, titulo, categoria from Libros where "+
				"isbn="+ isbn+"";
		DataBaseHelper<Libro> helper= new DataBaseHelper<Libro>();
		List<Libro> listaDeLibros;
		
		try { listaDeLibros= helper.seleccionarRegistros(consultaSQL,Libro.class);
		}catch(ClassNotFoundException e) {
			throw new DataBaseException("Error al leer el driver", e );
		}catch(SQLException e){
			throw new DataBaseException("Error al ejecutar consulta", e );
		}catch(IllegalAccessException e){
			throw new DataBaseException("Error acceso incorrecto a la clase", e );
		}catch(InvocationTargetException e ){
			throw new DataBaseException("Error reflection", e );
		}catch(InstantiationException e){
			throw new DataBaseException("Error reflection", e );
		}
		
		return listaDeLibros.get(0);
	}
	
	public static List<Libro> buscarPorCategoria(String categoria) 
	{
		String consultaSQL = "select isbn, titulo,categoria from Libros where categoria='"+
				categoria+"'";
		DataBaseHelper<Libro> helper= new DataBaseHelper<Libro>();
		List<Libro> listaDeLibros;
		try {		listaDeLibros= helper.seleccionarRegistros(consultaSQL, Libro.class);
		}catch(ClassNotFoundException e) {
			throw new DataBaseException("Error al leer el driver", e );
		}catch(SQLException e){
			throw new DataBaseException("Error al ejecutar consulta", e );
		}catch(IllegalAccessException e){
			throw new DataBaseException("Error acceso incorrecto a la clase", e );
		}catch(InvocationTargetException e ){
			throw new DataBaseException("Error reflection", e );
		}catch(InstantiationException e){
			throw new DataBaseException("Error reflection", e );
		}
		return listaDeLibros;
	}
} 
